package cz.oktogen.anna

class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }
}