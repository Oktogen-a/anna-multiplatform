plugins {
    java
    kotlin("jvm")
}

repositories {
    mavenLocal()
    maven {
        url = uri("https://repo.maven.apache.org/maven2/")
    }

    maven {
        url = uri("https://maven.vaadin.com/vaadin-addons")
    }
}

dependencies {
    implementation("com.vaadin:vaadin:14.7.1")
    implementation("com.vaadin:vaadin-spring-boot-starter:14.7.1")
    implementation("org.vaadin.artur:a-vaadin-helper:1.7.1")
    implementation("org.springframework.boot:spring-boot-starter-validation:2.4.5")
    implementation("org.springframework.boot:spring-boot-devtools:2.4.5")
    implementation("org.springframework:spring-webmvc:5.3.9")

    testImplementation("org.springframework.boot:spring-boot-starter-test:2.4.5")
    testImplementation("com.vaadin:vaadin-testbench:14.7.1")
    testImplementation("org.junit.vintage:junit-vintage-engine:5.7.1")
    testImplementation("io.github.bonigarcia:webdrivermanager:3.8.1")
}

group = "cz.oktogen.anna.webapp"
version = "1.0-SNAPSHOT"
description = "Project base for Spring Boot and Vaadin Flow"
java.sourceCompatibility = JavaVersion.VERSION_1_8

tasks.withType<JavaCompile>() {
    options.encoding = "UTF-8"
}
