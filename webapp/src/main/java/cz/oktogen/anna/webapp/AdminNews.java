package cz.oktogen.anna.webapp;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;

/**
 * A Designer generated component for the admin-news template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 */
@Tag("admin-news")
@JsModule("./admin-news.js")
@Route("")
public class AdminNews extends PolymerTemplate<AdminNews.AdminNewsModel> {

    @Id("button")
    private NativeButton button;
    @Id("test_button")
    private Button test_button;

    private int counter = 50;

    /**
     * Creates a new AdminNews.
     */
    public AdminNews() {
        // You can initialise any data required for the connected UI components here.

        Dialog dialog = new Dialog();
        dialog.add(new Text("Close me with the esc-key or an outside click"));

        dialog.setWidth("400px");
        dialog.setHeight("150px");

        test_button.addClickListener((ComponentEventListener<ClickEvent<Button>>) buttonClickEvent -> {
            counter = counter + 100;
            test_button.setText("Button click " + counter);
            test_button.setHeight(counter, Unit.PIXELS);
            test_button.setWidth(counter + 50, Unit.PIXELS);

            button.addClickListener(event -> dialog.open());
        });
    }

    /**
     * This model binds properties between AdminNews and admin-news
     */
    public interface AdminNewsModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}
